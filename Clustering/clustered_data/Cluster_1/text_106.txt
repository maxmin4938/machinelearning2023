Naive Bayes bir probabilistik sınıflandırma modelidir Bu model verilen bir girdi olasılıklarının olası sınıf etiketleri ile ilişkili olduğu varsayımını kullanarak tahmin yapar
Naive Bayes özellikler arasında bir bağımlılık varsayımı yapmaz Bu nedenle naive saf olarak adlandırılır Bu varsayım özellikler arasında bir ilişki olmadığını ve her bir özellik ayrı ayrı değerlendirildiği anlamına gelir
Naive Bayes genellikle metin sınıflandırması eposta spam filtreleme günlük dil işleme ve benzeri uygulamalarda kullanılır
Naive Bayes iki temel türde kullanılabilir Gaussian Naive Bayes ve Multinomial Naive Bayes Gaussian Naive Bayes sürekli değişkenli özellikler için kullanılırken Multinomial Naive Bayes sınıflandırma problemlerinde kullanılır
Fri Jan    GMT GMT
httpschatopenaicomchat